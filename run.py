#!/usr/bin/env python
"""This script reads *.CSV file and create the release_notes.txt."""

import os
import re

__author__ = "Andrew Emad"
__version__ = "0.1"


class Release_Notes:
    def __init__(self, config_file, output_file):
        self.__config_file = config_file
        self.__output_file = output_file
        self.text = ""
        self.input_fields = {}

    @staticmethod
    def get_key(line):
        """ Function returns key value from given line according to specific *.CSV format.
            (i.e. key,value\n)

            Args:
                - param1 (str): Line to be parsed

            Returns:
                - str: Key name """
        return line.split(',')[0]

    @staticmethod
    def get_value(line):
        """ Function returns value from given line according to specific *.CSV format.
            (i.e. key,value\n)

            Args:
                - param1 (str): Line to be parsed

            Returns:
                - str: Value of value element """
        return line.split(',')[1].rstrip()

    @staticmethod
    def print_title(self, name):
        """ Function adds given title to release notes text.

            Args:
                - param1 (str): Name of the section

            Returns:
                - Nothing """
        dashes = '+' + '=' * 112 + '+'
        spaces_left = (((len(dashes) - len(name)) / 2) - 1)
        spaces_right = (((len(dashes) - len(name)) / 2) - 1) + ((len(dashes) - len(name)) % 2)
        self.text += dashes + '\n'
        self.text += '|' + ' ' * spaces_left + name + ' ' * spaces_right + '| ' + '\n'
        self.text += dashes + '\n'

    @staticmethod
    def print_content(self, words):
        """ Function adds given words to a single line to release notes text.

            Args:
                - param1 (str): Words to be printed per line

            Returns:
                - Nothing """
        self.text += words + '\n'

    @staticmethod
    def print_list(self, key, type='- '):
        """ Function adds numbered list to release notes text & gets its values from the input config file.

            Args:
                - param1 (key in input_fields dictionary): key to get its data from input file

            Returns:
                - Nothing """
        if key in self.input_fields:
            list = self.input_fields[key]
            for i, value in enumerate(list):
                if value != '':
                    self.print_item(self, value, type, i)

    @staticmethod
    def print_input_list(self, list, type='- '):
        """ Function adds given list with '-' at the start of each element to release notes text
            & gets its values from already added/read elements or returns default value.

            Args:
                - param1 (list): List to be added

            Returns:
                - Nothing """
        for i, key in enumerate(list):
            self.print_item(self, key + ': ' + self.input_fields[key], type, i)

    @staticmethod
    def print_item(self, value, type, number):
        """ Function adds single item from a list to release notes text.

            Args:
                - param1 (str): value of the item
                - param2 (str): bullet type format
                - param3 (int): number of the item in the list

            Returns:
                - Nothing """
        if any(char.isdigit() for char in type):
            split = type.split('1')
            self.text += split[0] + str(number + 1) + split[1] + value + '\n'
        else:
            self.text += type + value + '\n'

    def add_elements(self, keys, default_value):
        """ Function adds list of elements to input_fields of the release notes beside the read ones from the config file.

            Args:
                - param1 (list): List of needed fields
                - param2 (str): Default values if not fed through input file

            Returns:
                - Nothing """

        for i in range(len(keys)):
            if keys[i] not in self.input_fields:
                self.input_fields[keys[i]] = default_value

    def add_custom_element(self, key, value):
        """ Function adds custom element to input_fields of the release notes
            & returns the value to be used later if needed.

            Args:
                - param1 (str): Key for the custom element
                - param2 (str): Value for the custom element

            Returns:
                - str: Value of the added custom element """
        self.input_fields[key] = value
        return value

    def add_section(self, section, **kwargs):
        """ Function adds section to the release notes with its content.

            Args:
                - param1 (str): Section title name
                - param2 (keyworded variable arguments): - input_list --> for custom list for the section
                                                         - text_# --> for a single line in the section
                                                         - type --> for bullet type for the list in the section

            Returns:
                - Nothing """
        self.print_title(self, section)
        for i in range(len(kwargs)):
            if 'text_%s' % (i + 1) in kwargs:
                self.print_content(self, kwargs['text_%s' % (i + 1)])
        if section in self.input_fields:
            if 'type' in kwargs:
                self.print_list(self, section, kwargs['type'])
            else:
                self.print_list(self, section)
        if 'input_list' in kwargs:
            if 'type' in kwargs:
                self.print_input_list(self, kwargs['input_list'], kwargs['type'])
            else:
                self.print_input_list(self, kwargs['input_list'])

    def get_content(self):
        """ Function reads input *.CSV and saves data into input_fields variable.

            Args:
                - Nothing

            Returns:
                - Nothing """
        sections_lines = []
        with open(self.__config_file) as f:
            lines = f.readlines()
        for i, line in enumerate(lines):
            if re.match(',\n', line):
                sections_lines.append(i)
        sections_lines.append(i)

        for i in range(sections_lines[0]):
            key = self.get_key(lines[i])
            value = self.get_value(lines[i])
            self.input_fields[key] = value

        for i in range(len(sections_lines) - 1):
            key = self.get_key(lines[sections_lines[i] + 1])
            self.input_fields[key] = []
            for j in range(sections_lines[i], sections_lines[i + 1]):
                self.input_fields[key].append(self.get_value(lines[j + 1]))

    def write(self):
        with open(self.__output_file, 'w') as f:
            f.write(self.text)


def main():
    """ Main function to create the release notes.
        Main steps to be followed:
            1. Create Instance of release notes.
            2. Read content of input file.
            3. Add elements if needed.
            4. Add sections as required.
    """

    """ List of SW versions to be printed. """
    sw_version = ['Baseline version/tag',
                  'Host SW version',
                  'Host Bootloader SW version',
                  'Extended Host Bootloader SW version',
                  'SoC SW version',
                  'SoC Bootloader SW version',
                  'Fusion SW version',
                  'SoC platform SW version',
                  'Host platform SW version',
                  'Kernel SW version (Integrity)']

    references = ['Tag name',
                  'Release package']

    """ 1. Instance of release notes class with config.csv as input file. """
    release_notes = Release_Notes('config.csv', 'release_notes.txt')

    """ 2. Read content of input *.csv file. """
    release_notes.get_content()

    """ 3. Add list of SW versions with N/A as default value. """
    release_notes.add_elements(sw_version, 'N/A')
    release_notes.add_elements(references, 'N/A')

    if release_notes.input_fields['Fusion SW version'] != 'N/A':
        baseline_number = release_notes.add_custom_element('Baseline version/tag',
                                                           release_notes.input_fields['SoC SW version']
                                                           + '_' + release_notes.input_fields['Fusion SW version'])
    else:
        baseline_number = release_notes.add_custom_element('Baseline version/tag',
                                                           release_notes.input_fields['SoC SW version'])

    project_name_to_link = release_notes.input_fields['Project Name'].lower().replace(' ', '_')
    package_name_to_link = re.sub('\s|\.', '_', release_notes.input_fields['Package Name'].lower())
    sprint_name = release_notes.input_fields['Sprint Name']

    if release_notes.input_fields['Project Name'] != 'N/A':
        release_notes.add_custom_element('Tag name', baseline_number + '\n'
                                         '    |-> Link: https://forge.vnet.valeo.com/ctf/code/git/projects.'
                                         + project_name_to_link + '/scm.' + project_name_to_link + '/tags/' +
                                         baseline_number + '/view\n')

    if release_notes.input_fields['Package Name'] != 'N/A':
        temp_baseline_number = re.sub('\s|\.', '_', baseline_number.lower())
        release_notes.add_custom_element('Release package', baseline_number + '.zip\n'
                                         '    |-> Link: https://forge.vnet.valeo.com/sf/frs/do/viewRelease/projects.'
                                         + project_name_to_link + '/frs.' + package_name_to_link + '.' +
                                         temp_baseline_number)

    """ 4. Adding needed sections. """
    release_notes.add_section('Introduction',
                              text_1='This document contains the introduced SW changes in this release package of ' +
                                     release_notes.input_fields['Project Name'] + ' project.',
                              text_2='Also this document has the known issues in this release if any occurs.')

    release_notes.add_section('Description',
                              text_1='This release is for ' + baseline_number + ' (' + sprint_name +
                                     ') has the following:',
                              type='    1. ')

    release_notes.add_section('SW Versions',
                              input_list=sw_version)

    release_notes.add_section('List of changes',
                              type='1. ')

    release_notes.add_section('References',
                              input_list=references)

    release_notes.add_section('Known Issues')

    release_notes.add_section('Additional Info')

    release_notes.write()


if __name__ == "__main__":
    main()
    success_msg = "Release notes has been created in " + os.getcwd() + " !"
    print (success_msg)
    print ('-' * len(success_msg))
    os.system("pause")
